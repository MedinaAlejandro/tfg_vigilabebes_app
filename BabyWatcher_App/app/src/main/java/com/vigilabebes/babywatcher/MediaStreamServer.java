package com.vigilabebes.babywatcher;

import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MediaStreamServer {

    static final int frequency = 44100;
    static final int channelConfiguration = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    static final int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    boolean isRecording;
    int recBufSize;
    BluetoothSocket connfd;
    InputStream myInputStream;
    OutputStream myOutputStream;

    AudioRecord audioRecord;

    //Constructor:
    public MediaStreamServer(final Context ctx, final BluetoothSocket socket) {

        connfd = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;


        try {
            tmpIn = connfd.getInputStream();
            tmpOut = connfd.getOutputStream();
        } catch (IOException e) {
        }
        myInputStream = tmpIn;
        myOutputStream = tmpOut;
        recBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, audioEncoding);
        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, audioEncoding, recBufSize);


        new Thread() {
            byte[] buffer = new byte[recBufSize];
            //byte[] buffer= new byte[7072];
            //byte[] buffer= new byte[990];


            @Override
            public void run() {
                super.run();
                audioRecord.startRecording();
                isRecording = true;

                while (isRecording) {
                    int readSize = audioRecord.read(buffer, 0, recBufSize);
                    //int readSize=audioRecord.read(buffer,0,buffer.length);

                    try {
                        //myOutputStream.write(buffer,0,readSize);
                        connfd.getOutputStream().write(buffer);
                    } catch (IOException e) {
                    }

                    String RecordMinBuff=Integer.toString(recBufSize);

                    Log.d("RecordMinBuff",RecordMinBuff);
                    //audioRecord.stop();
                    //audioRecord.release();

                }
                audioRecord.stop();
                audioRecord.release();
                try {
                    connfd.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void stop() {

        isRecording=false;
    }
}
