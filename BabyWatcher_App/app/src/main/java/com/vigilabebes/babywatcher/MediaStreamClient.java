package com.vigilabebes.babywatcher;

import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MediaStreamClient {

    //Declarando variables:
    static final int frequency = 44100;
    static final int channelConfiguration = AudioFormat.CHANNEL_CONFIGURATION_STEREO;
    static final int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    boolean isPlaying;
    int playBufSize; // Tamaño del Buffer
    BluetoothSocket connfd;
    InputStream myInputStream;
    OutputStream myOutputStream;
    AudioTrack audioTrack; // Variable de Audio

    //Constructor:
    public MediaStreamClient(final Context ctx, final BluetoothSocket socket) {
        connfd = socket;
        playBufSize = AudioTrack.getMinBufferSize(frequency, channelConfiguration, audioEncoding);


// DEJAMOS ESTA PARTE COMMENTADA
        /*

        InputStream tmpIn = null;
        OutputStream tmpOut = null;


        try {
            tmpIn = connfd.getInputStream();
            tmpOut = connfd.getOutputStream();
        } catch (IOException e) {
        }
        myInputStream = tmpIn;
        myOutputStream = tmpOut;

        playBufSize = AudioTrack.getMinBufferSize(frequency, channelConfiguration, audioEncoding);

        // Creamos el Audio Track pasandole las variables necesarias.. ESPECIAL ATENCION en la variable playBufSize!!! creo que en esta está el problema

        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, frequency, channelConfiguration, audioEncoding, playBufSize, AudioTrack.MODE_STREAM);
        audioTrack.setStereoVolume(100, 100);

        */
        //HASTA AQUI ES LO COMENTADO

        new Thread() {
            //byte[] buffer = new byte[playBufSize];
            //byte [] buffer=new byte[4096];
            //byte [] buffer=new byte[990];

            int contador=0;
            int readSize = 0;


            @Override
            public void run() {
                super.run();
               // audioTrack.play();
                //isPlaying = true;

                try {
                    // Creamos Buffer para albergar el incoming data
                    byte[] audioBuffer = new byte[4096];

                    //Creamos el input stream para leer los datos que nos llegan
                    BufferedInputStream myBis = new BufferedInputStream(connfd.getInputStream());
                    DataInputStream myDis=new DataInputStream(myBis);

                    Log.d("BuffInputStream", "Creado el Input Stream");

                    //Creamos el AudioTrack
                    AudioTrack myAudioTrack=new AudioTrack(AudioManager.STREAM_MUSIC, frequency, channelConfiguration, audioEncoding, playBufSize, AudioTrack.MODE_STREAM);
                    Log.d("AudioTrack", "Creado el Track");

                    // Leer el Audio
                    int i=0;
                    // No estoy seguro de las condiciones del Loop
                    while (connfd.getInputStream().read(audioBuffer)!=-1) {
                        audioBuffer[audioBuffer.length - 1 - i] = myDis.readByte();
                        myAudioTrack.play();
                        myAudioTrack.write(audioBuffer, 0, audioBuffer.length);
                        i++;
                    }





                }catch (IOException e1) {
                    e1.printStackTrace();
                }





// COMENTO TODO ESTO PARA PROBAR UNA COSA NUEVA A VER SI FUNCIONA!!!
 /*

                while (isPlaying) {
                    //int readSize = 0;
                    try {
                        readSize = myInputStream.read(buffer);


                        // Idea Crear el Buffer aquí con tamaño ReadSize cada vez que hace el bucle por si cada vez lee algo diferente

                    } catch (IOException e) {
                    }

                    // IDEA??? Poner aquí otro buffer de tamaño PlayBuffSize que se vaya llenando de lo que lee el InputStream hasta que se llene
                    //y encontces es cuando se escribe en el audiotrack??

                    //contador=contador+readSize;
                   // if(contador==playBufSize) {

                        if(contador==0){
                            int write = audioTrack.write(buffer, 0, readSize);}

                        else{
                            int contador2=contador+readSize;
                            int write = audioTrack.write(buffer, contador,contador2);}
                        contador+=readSize;


                        String PlayBuffString = Integer.toString(playBufSize);
                        String ReadBuffString = Integer.toString(readSize);
                        //String WriteAudio = Integer.toString(write);
                        //String MaxReceiver=Integer.toString(MaxReceivePack);

                        Log.d("PlayBuff", PlayBuffString);
                        Log.d("ReadBuffSize", ReadBuffString);
                        //Log.d("WriteBytes", WriteAudio);
                        //Log.d("MaxReceiver",MaxReceiver);

                        //audioTrack.flush();
                        contador=0;


                    } else {
                        String ReadBuffString = Integer.toString(readSize);
                        String ContadorString =Integer.toString(contador);
                        Log.d("Contador Pequeño:",ContadorString);
                        Log.d("Contador ReadSize:",ReadBuffString);
                    }





                }
                audioTrack.stop();
                try {
                    connfd.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
 */  // COMENTO TODO LO DE ARRIBA PARA PROBAR UNA COSA NUEVA A VER SI FUNCIONA!!!

            }


        }.start();
    }
    public void stop(){
        isPlaying=false;
    }



}