package com.vigilabebes.babywatcher;
//Here we go again

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {

    //Miembros Elementos del Bluetooth
    private BluetoothAdapter BtAdapter;
    private BluetoothDevice BtDevice;
    private BluetoothSocket BtClientSocket=null;
    private BluetoothServerSocket BtServerSocket;

    //UUID y Name para el Socket
    private UUID myUUID=UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private String myNAME="BabyWatche";




    //Miembros Elementos del LayOut
    private Button BtnSend;
    private Button BtnScan;
    private Button BtnStop;
    private Button BtnServer;
    private TextView Display;
    private EditText WriteSplot;
    private ListView PairedList;

    // ArrayAdapter para la Lista
    private ArrayAdapter mPairedArray;

    // Broadcast Receiver e Intent Filter para que interprete los mensajes recibidos mientras escanea
    BroadcastReceiver mScanReceiver;
    IntentFilter ScanIntentFilter;

    //Thread que controla la conexión
    public ManageConectionThread ConectedThread;

    // Handler para que el Thread se comunique con el Main
    Handler mHandler;
    private static final int MESSAGE_READ = 100;
    private static final int MESSAGE_WRITE = 101;

    //String Buffer
    private StringBuffer mOutStringBuffer;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Vinculando con Elemntos del LayOut
        BtnSend=findViewById(R.id.buttonSend);
        BtnScan=findViewById(R.id.buttonScan);
        BtnStop=findViewById(R.id.buttonStop);
        BtnServer=findViewById(R.id.buttonServer);
        Display=findViewById(R.id.DisplayView);
        WriteSplot=findViewById(R.id.WritedText);
        PairedList=(ListView) findViewById(R.id.List);

        // ArrayAdapter para la Lista
        mPairedArray=new ArrayAdapter(this, R.layout.device_name);


        // Inicializar Bluetooth Adaparter
        BtAdapter=BluetoothAdapter.getDefaultAdapter();
        //Comprobar si el Bluetooth Esta Activado y que en caso contrario solicite su activación
        checkBTState(BtAdapter);

        //Cargar la Lista de Dispositivos Bluetooth previamente enlazados con nuestro terminal
        PairedList.setAdapter(mPairedArray);
        // Comportamiento al tocar un elemento de la Lista: se intenta conectar a dicho elemento
        PairedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String info=((TextView)view).getText().toString();
                String address=info.substring(info.length()-17);
                // Crea el Dispositivo Bluetooth a partir del Address seleccionado
                BtDevice=BtAdapter.getRemoteDevice(address);
                // BluetoothDevice SelectedDevice=BtAdapter.getRemoteDevice(address);


                // Crear el Client Socket
                ClientSocket(BtDevice);

            }
        });

        // ESTO ES MUY IMPORTANTE!!!!! sin que pida la ubicación no es capaz de encontrar dispositivos al Escanear
        // Esto ocurre desde la versión 6.0 de Android
        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);


        // Definiendo Broadcast Receiver para el escaneo de nuevos Dipositivos
        mScanReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action=intent.getAction();

                // When discovery finds a device
                if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action))
                    Toast.makeText(context, ".....Starting Scan....", Toast.LENGTH_SHORT).show();
                else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    Toast.makeText(context, ".....Scanning Finished....", Toast.LENGTH_SHORT).show();
                    mPairedArray.add("End of Scan");
                }
                else if(BluetoothDevice.ACTION_FOUND.equals(action)){
                    Toast.makeText(context, ".....Device Found....", Toast.LENGTH_SHORT).show();
                    // Get BluetoothDevice object from Intent
                    BluetoothDevice device=intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    String DeviceData=device.getName()+ "\n" +device.getAddress();
                    mPairedArray.add(DeviceData);

                }
            }

        };
        // Definiendo IntentFilter
        ScanIntentFilter=new IntentFilter();
        ScanIntentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        ScanIntentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        ScanIntentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        ScanIntentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);


    }

    @Override
    protected void onResume() { //Que hacer cuando minimizamos y volvemos a maximizar
        super.onResume();

        // Comprueba que no se ha desconectado el Bluetooth
        checkBTState(BtAdapter);
        //GetBondedDevices(PairedList,BtAdapter);

        // Setting the Receiver
        registerReceiver(mScanReceiver, ScanIntentFilter);

        mPairedArray.clear(); // Limpia la lista por lo que no vemos los Dispositivos previamente enlazados. Hay que cambiarlo

        // Definición del Handler que comunica el ConectionThread con el Main. Útil cuando se trataba de enviar Strings
        mHandler = new Handler() {
            public void handleMessage (Message msg){
                if(msg.what==MESSAGE_READ){

                    byte[] readBuf;

                    readBuf=(byte []) msg.obj;
                    String readMessage=new String(readBuf,0,msg.arg1);
                    //readMessage=new String(readBuf);
                    Display.setText(readMessage);
                    mPairedArray.add(readMessage);

                }
                else if(msg.what==MESSAGE_WRITE){
                    byte[] writeBuf=(byte[]) msg.obj;
                    String writeMessage=new String(writeBuf,0,msg.arg1);
                    //Display.setText(writeMessage);

                }

            }


        };

        mOutStringBuffer=new StringBuffer("");

    }

// Checkea el Estado del Blutooth, y si no está activado te pide que lo actives
    public void checkBTState(BluetoothAdapter mBTAdapter) {

        if(mBTAdapter==null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta Bluetooth", Toast.LENGTH_SHORT).show();
        } else {
            if (mBTAdapter.isEnabled()) {
                Toast.makeText(getBaseContext(), ".....Bluetooth Activated....", Toast.LENGTH_SHORT).show();

            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);

            }
        }
    }
    // Muestra los dispisitivos previamente enlazados con nuestro Smartphone
    public void GetBondedDevices(ListView Lista, BluetoothAdapter BTAdapter){
        // Adquirir Bonded Devices con nuestro Terminal
        Set<BluetoothDevice> PairedDevices=BTAdapter.getBondedDevices();
        // Añadir a la Lista
        if(PairedDevices.size()>0) {
            for (BluetoothDevice device : PairedDevices)
                mPairedArray.add(device.getName() + "\n" + device.getAddress());
        } else {
            String noDevices = "Ningun Dispositivo asociado";
            mPairedArray.add(noDevices);
        }

        }
        // Metodo Scan para buscar nuevos Dispositivos Bluetooth
    public void Scan (View view) {
        mPairedArray.clear();
        Toast.makeText(getBaseContext(), "Escaneando", Toast.LENGTH_LONG).show();
        BtAdapter.startDiscovery();
    }

    // Metodo para crear Socket Cliente
    private void ClientSocket(BluetoothDevice device){
        BtAdapter.cancelDiscovery();
        BluetoothSocket tmp= null; //Socket temporal

        try {
            tmp = device.createRfcommSocketToServiceRecord(myUUID);

        }catch (IOException e){}
        BtClientSocket=tmp;
        try{
            BtClientSocket.connect();
            Toast.makeText(getBaseContext(), "Conexión al Socket con éxtito", Toast.LENGTH_SHORT).show();

        } catch (IOException e){
            Toast.makeText(getBaseContext(), "Error al Conectar Socket", Toast.LENGTH_SHORT).show();
        }
        if(BtClientSocket!=null){
            /*Solía llamar al ConnectedThread que funcionaba como un chat de texto para probar la conexión
            pero ahora creamos un objeto de la Clase MediaStreamClient y dicho objeto se encarga de el manejo
            del Audio*/

            //ConectedThread=new ManageConectionThread(BtClientSocket);
            //ConectedThread.start();

            MediaStreamClient msc=new MediaStreamClient(MainActivity.this,BtClientSocket);


        }else{
            Toast.makeText(getBaseContext(), "Error al Conectar Socket", Toast.LENGTH_SHORT).show();
        }
    } //Fin de ClienSocket

    //Metodo para crear Servidor Socket

    public void ServerSocket (View v){
        Toast.makeText(getBaseContext(), "Waiting Client", Toast.LENGTH_SHORT).show();
        BluetoothServerSocket tmp=null;

        //Hacerlo Visible para otros Dispositivos
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);
        //Bond con nuestro UUID
        try{
            tmp=BtAdapter.listenUsingRfcommWithServiceRecord(myNAME, myUUID);
        } catch(IOException e){
            Toast.makeText(getBaseContext(), "Error Listen", Toast.LENGTH_SHORT).show();
        }
        BtServerSocket=tmp;
        // Conexion con Cliente
        while (true){
            try{
                BtClientSocket=BtServerSocket.accept();
            } catch (IOException e){
                Toast.makeText(getBaseContext(), "Error en Accept", Toast.LENGTH_SHORT).show();
                break;
            }
            //Si Accepta la Conexion
            if(BtClientSocket!=null){
                Toast.makeText(getBaseContext(), "Conexion Aceptada", Toast.LENGTH_SHORT).show();
                mPairedArray.add("Conexion Aceptada");
                /*  Se llamaba al Thread que manejaba la conexión como un chat de texto pero ahora
                no hace falta */

                //ConectedThread=new ManageConectionThread(BtClientSocket);
                //ConectedThread.start();

                /* Igual que con el Cliente creamos un Objeto de la Clase MediaStreamServer que se encarga
                de la gestión del Audio*/

                final MediaStreamServer mss=new MediaStreamServer(MainActivity.this,BtClientSocket);



                try { //Cierra Socket Servidor
                    BtServerSocket.close();
                }catch (IOException e){
                    Toast.makeText(getBaseContext(), "Error al Cerra ServerSocket", Toast.LENGTH_SHORT).show();
                    break;
                }
                break;
            }
        }//Fin del While
    } //Fin de ServerSocket

    // Envia el mensaje
    public void SendMsg (View view){

        String Msg=WriteSplot.getText().toString();
        byte[] buffer;
        buffer=Msg.getBytes();
        mOutStringBuffer.setLength(0);
        WriteSplot.setText(mOutStringBuffer); // para que al enviar limpie el EditText
        ConectedThread.write(buffer);





    } //Fin de SendMsg

    //  Thread para el control de la conexión BLuetooth como chat de Texto
    private class ManageConectionThread extends Thread{

        private BluetoothSocket mySocket;
        InputStream myInputStream;
        OutputStream myOutputStream=null;
        //Habrá que poner mas cosas

        //Constructor
        public ManageConectionThread(BluetoothSocket socket){
            mySocket=socket;
            InputStream tmpIn=null;
            OutputStream tmpOut=null;


            try{
                tmpIn=mySocket.getInputStream();
                tmpOut=mySocket.getOutputStream();
            } catch(IOException e){}
            myInputStream=tmpIn;
            myOutputStream=tmpOut;

        }

        @Override
        public void run() {
            super.run();
            byte[] buffer=new byte[1024];
            int bytes=0;

            while(true){

                try{

                    // Lee desde el InputStream
                    bytes=myInputStream.read(buffer);
                    // Envia Mensaje usando el Handler
                    mHandler.obtainMessage(MESSAGE_READ,bytes,-1,buffer).sendToTarget();

                }catch (IOException e){}
                }

        }
        // Envia datos mediante OutpuStream
        public void write(byte[] buffer){
            try{
                myOutputStream.write(buffer);
            }catch (IOException e){}
        }

        public void cancel(){

            try {
                mySocket.close();
            }catch (IOException e){}

        }
    } //Fin del Thread

    public void onDestroy(){
        super.onDestroy();
        //Stop al Thread
        if(ConectedThread!=null)  ConectedThread.stop();
    }


} // Fin de MainActivity
