package com.example.medina.vigilabebes;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.exmaple.medina.vigilabebes.MESSAGE";

    private static final String TAG = "DeviceListActivity";



    //Bluetooth y array
    private BluetoothAdapter mBTAdapter;
    private ArrayAdapter mPairedDevicesArrayAdapter;


    //Declarando elementos del LayOut
    private Button mScanBtn;
    private Button mServerBtn;
    private Button mSendBtn;
    private Button mReadBtn;

    private TextView Display;


    //Declarando sockets
    private BluetoothSocket btClientSocket=null;
    private BluetoothServerSocket btServerSocket=null;

    private String NAME="Vigilabebes";
    private UUID mUUID=UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //Declarando Thread

    ConnectedThread conect;

    // Declarando HANDLER
     Handler mHandler;
     Message msg;
    private static final int MESSAGE_READ = 100;
    private static final int MESSAGE_WRITE = 101;




    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

        //DEFINICION DE HANDLER

          Handler mHandler = new Handler() {
            public void handleMessage (Message msg){
                if(msg.what==MESSAGE_READ){
                    byte[] readBuf=(byte []) msg.obj;
                    String readMessage=new String(readBuf,0,msg.arg1);
                    Display.setText(readMessage);


                }
                else if(msg.what==MESSAGE_WRITE){
                    byte[] writeBuf=(byte[]) msg.obj;
                    String writeMessage=new String(writeBuf,0,msg.arg1);
                   //Display.setText(writeMessage);

                }

            }


        };

    }




    @Override
    protected void onResume() {  //incialemnete estaba en onCreate lo cambio a ver si se arreglo  el problema
        super.onResume();
        setContentView(R.layout.activity_main); // Esto debe ir arriba si queremos que al minimizar y maximizar la APP no se vuelva a la pantalla de incio

        //Funcion para comprobar si el BT está activado
        checkBTState();
        //Inicializa el array adapter
        mPairedDevicesArrayAdapter=new ArrayAdapter(this, R.layout.device_name);

        //Inicializar el ListView
        ListView pairedListView=(ListView) findViewById(R.id.Lista);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        //obtener el adaptador bluetooth

        mBTAdapter = BluetoothAdapter.getDefaultAdapter();

        //Obtener los devices emparejados
        Set <BluetoothDevice> pairedDevices=mBTAdapter.getBondedDevices();

        //Añadir los devices emparejados a la lista
        if(pairedDevices.size()>0){
            for (BluetoothDevice device : pairedDevices){
                mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        } else{
            String noDevices="Ningún Dispositivo asociado";
            mPairedDevicesArrayAdapter.add(noDevices);
        }


    //Enlazando elementos del LayOut
        mScanBtn 			= findViewById(R.id.BotonScan);
        mServerBtn=findViewById(R.id.BotonServer);

        mSendBtn=findViewById(R.id.BotonSend);
        mReadBtn=findViewById(R.id.BotonRead);

        Display=findViewById(R.id.MsgTextView);

        //InputStream tmpIn=null;
        //OutputStream tmpOut=null;



        IntentFilter filter = new IntentFilter();

        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        registerReceiver(mReceiver, filter);





    }
    //Definición para que al tocar uno de los Dispositivos Bluetooth de la Lista se conecte a él

    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView av, View v, int arg2, long arg3) {



            // Conseguir la dirección del dispositivo Bluetooth a partir de los ultimos 17 caracteres leidos
            mPairedDevicesArrayAdapter.clear();
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);
            // Se realiza la conexión socket con un cliente
            BluetoothDevice deviceselected=mBTAdapter.getRemoteDevice(address);
            CrearCliente(deviceselected);

        }




    };
    // Funcion para comprobar si el dipositivo dispone de Bluetooth, un poco innesesario pues actualmente todos los Smartphones tienen Bluetooth
    // Pero se aprovecha para que el sistema le pida al usuario que active el Bluetooth en caso de que esté desactivado
    public void checkBTState() {
        mBTAdapter=BluetoothAdapter.getDefaultAdapter();
        if(mBTAdapter==null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta Bluetooth", Toast.LENGTH_SHORT).show();
        } else {
            if (mBTAdapter.isEnabled()) {
                Log.d(TAG, "...Bluetooth Activado...");
            } else { //intent para que se active Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);

            }
        }
    }


    // Crear a BroadcastReceiver para ACTION_FOUND para que al encontrar un dispositivo lo añada a la Lista
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
                mPairedDevicesArrayAdapter.add("Buscando....");

            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                mPairedDevicesArrayAdapter.add("Terminó de Buscar");
            }
            else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                mPairedDevicesArrayAdapter.add("Encontrado");
                // Consegui un Objeto BluetoothDevice
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
               mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        }
    };

    //------Funcion para crear un Server util para comprobar si funciona en el envio de datos por Bluetooth usando dos Smartphones
    public void CrearServer(View view){
        mPairedDevicesArrayAdapter.add("Esperando Cliente");
        BluetoothServerSocket tmp=null;
        byte[] buffer=new byte[1024];
        int numbytes;

        //Hacer Visible para otros Dispisitivos

        Intent discoverableIntent = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);


        InputStream tmpIn=null;


            try {
                tmp = mBTAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME, mUUID);

            }catch (IOException e){  }
            btServerSocket=tmp;

            while(true){
                try{

                   // socketFinal=btServerSocket.accept();
                    btClientSocket=btServerSocket.accept();

                    btServerSocket.close();
                }catch (IOException e){
                    break;
                }
                //Si se conecta
                if(btClientSocket!=null){
                    mPairedDevicesArrayAdapter.clear();
                    mPairedDevicesArrayAdapter.add("socket realizado con exito");

                    //Arrancar Hilo cuando actua como Servidor

                    conect=new ConnectedThread(btClientSocket);
                    conect.start();
                    break;
                }

        }
        if (btClientSocket.isConnected()){ Display.setText("Socket Conectado");}
        else{ Display.setText("Socket No Conectado");}

    }

        //--Funcion para cliente
    public void CrearCliente (BluetoothDevice Device) {
        BluetoothSocket tmp = null;
        BluetoothDevice mmDevice = Device;

        try {
            tmp = Device.createRfcommSocketToServiceRecord(mUUID);

        } catch (IOException e) { }
        btClientSocket=tmp;
        try{
            btClientSocket.connect();
            // Arranca el Hilo cuando se realiza la conexion
            conect=new ConnectedThread(btClientSocket);
            conect.start();



        }catch(IOException connectExecption){

            mPairedDevicesArrayAdapter.add("Coneccion fallida");
           // btClientSocket.close();

        }
        if (btClientSocket.isConnected()){
            Display.setText("Socket Conectado");
        }else{
            Display.setText("Socket No Conectado");
        }
        mPairedDevicesArrayAdapter.add("Final de Crear Cliente"); //Avisa de que concluye


    }

    // Escanear nuevo Dispositivo que no este en la lista de ya emparejados
    public void StartScan(View view) {

        mPairedDevicesArrayAdapter.clear();
        mPairedDevicesArrayAdapter.add("Escaneando");
        mBTAdapter.startDiscovery();

    }

    // Para el Escaneo
    public void StopScan(View view){
        mBTAdapter.cancelDiscovery();
        mPairedDevicesArrayAdapter.add("Cancelando");


    }

    //Funcion para enviar texto por Bluetooth
    public void SendMsg (View view){

        EditText escritura=(EditText) findViewById(R.id.IDEscritura);
        String MsgEscrito=escritura.getText().toString();

        byte[] bytesMsg= MsgEscrito.getBytes();
        conect.write(bytesMsg);

    }

    //Funcion para recibir mensaje por Bluetooth Manualmente en lugar de que el Thread "escuche"
    public void ReadMsg(View view){

        mPairedDevicesArrayAdapter.clear();
        String Msg="";

        //Mensaje que se muestra en pantalla para comprobar que la conexion no se ha perdido
        if (btClientSocket.isConnected()){
            Msg="Conectado";




        }else  Msg="Desconectado";

        Display.setText(Msg);


        InputStream tmpIn=null;
        byte[] buffer=new byte[1024];
        int numbytes;


        try{
            tmpIn=btClientSocket.getInputStream();
        } catch (IOException e){}


        try{
            numbytes=tmpIn.read(buffer);
            String Mensaje=new String(buffer);

            mPairedDevicesArrayAdapter.add(Mensaje);
            Display.setText(Mensaje);
        }catch (IOException e){}
        mPairedDevicesArrayAdapter.add("Mensaje Recibido");


    }

    //Thread para enviar y escribir


    private class ConnectedThread extends Thread {
        private  BluetoothSocket mmSocket;
        private  InputStream mmInStream;
        private  OutputStream mmOutStream;
        int aviableBytes=0;
        Context context=getApplicationContext();
        int duration= Toast.LENGTH_LONG;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            // Definir los Input y Output Steams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024]; //Almacena el Stream
            int bytes;
            // Bucle para que el Thread lo ejecute constantemente
            while(true){
                // Comprueba si la conexion está activa
                if(btClientSocket.isConnected()) {
                    //Realiza la lectura
                    try {
                        bytes = mmInStream.read(buffer);
                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget();


                    } catch (IOException e) {
                        break;
                    }
                }else{
                    Display.setText("No Conectado");
                }

            }
        }

        // Funcion para enviar contenido
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
                mHandler.obtainMessage(MESSAGE_WRITE, -1, -1, bytes)
                        .sendToTarget();

            } catch (IOException e) { }
        }

        // Funcion Cancelar
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

}
