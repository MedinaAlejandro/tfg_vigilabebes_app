package com.example.testingthings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class MainActivity extends AppCompatActivity {

    private Socket s;
    public  Socket socket;
    private static Button BtnConect;
    public static TextView TxtView;
    private static String ip="192.168.1.42";
    private SocketAddress sAdd;
    private static int port=50017;

    Thread Thread1=null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Viculando Elementos del LayOut
        BtnConect=findViewById(R.id.button_Conect);
        TxtView=findViewById(R.id.DisplayView);



    }

    public void Conectar (View v){

       /*
        try{

           //InetAddress serverAddr=InetAddress.getByName(ip);

            s=new Socket(ip,port);
            //s.connect();

        }catch(IOException e){

           // Log.e("Socket","Error al conectar");
        }
        if (s!=null){
            TxtView.setText("Conectado");
        }
        */

       //Probamos con Thread
        Thread1=new Thread(new Thread1());
        Thread1.start();


    }

    // PROBAMOS CON UN THREAD

    class Thread1 implements Runnable {
        public void run() {
            Socket socket;
            try {
                socket = new Socket(ip, port);

                if (socket.isConnected()){
                    TxtView.setText("Conectado");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //tvMessages.setText("Connected\n");
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
